﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace BMPImageTest
{
    public partial class Form1 : Form
    {
        String curFileName;
        Bitmap curBitmap;

        public Form1()
        {
            InitializeComponent();
        }

        private void IDM_FILE_OPEN_Click(object sender, EventArgs e)
        {
            OpenFileDialog fopend = new OpenFileDialog();
            fopend.Filter = "所有图像文件|*.bmp;*.pcx;*.png;*.jpg;*.gif;*.tif;*.ico|"
                     + "位图(*.bmp;*.jpg;*.png)|" +
                     "矢量图()*wmf";
            fopend.Title = "打开图像文件";
            fopend.ShowHelp = true;

            if (fopend.ShowDialog() == DialogResult.OK)
            {
                curFileName = fopend.FileName;

                try
                {
                    curBitmap = (Bitmap)Image.FromFile(curFileName);
                }
                catch (Exception exp)
                {
                    MessageBox.Show(exp.Message);
                }

            }
            //刷新窗口，重新绘制，执行paint()方法
            Invalidate();

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

            // MessageBox.Show(curFileName);

        }


        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            if (curBitmap != null)
            {
                // g.DrawImage(curBitmap, 100, 100, curBitmap.Width, curBitmap.Height);
                pictureBox1.Image = curBitmap;

                //pictureBox1.Width = curBitmap.Width;
                //pictureBox1.Height = curBitmap.Height;
            }
        }



        private void Form1_Load(object sender, EventArgs e)
        {

        }
        /*图片平移操作*/
        /*图片平移操作*/
        /*图片平移操作*/
        /*图片平移操作*/
        /*图片平移操作*/

        private void IDM_IMGMOVE_Click(object sender, EventArgs e)
        {
            DlgPicuterMove dpm = new DlgPicuterMove();
            if (dpm.ShowDialog() == DialogResult.OK)
            {
                int dx = Convert.ToInt32(dpm.XOffset);
                int dy = Convert.ToInt32(dpm.YOffset);
                // MessageBox.Show(dpm.XOffset + "," + dpm.YOffset);
                ImageMove(curBitmap, dx, dy);
            }
        }

        //图像平移处理
        public void ImageMove(Bitmap bitmap, int dx, int dy)
        {
            //创建位图矩形区域 
            Rectangle rect = new Rectangle(0, 0, bitmap.Width, curBitmap.Height);

            //锁定图像数据区域
            System.Drawing.Imaging.BitmapData bmpdata = bitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, curBitmap.PixelFormat);

            //获取独享数据区域的首地址
            IntPtr ptr = bmpdata.Scan0;

            //计算机图像数据占用的内存大小，病分配数据处理缓冲空间
            int bytes = bmpdata.Stride * bmpdata.Height;
            byte[] rgbValues = new byte[bytes];
            byte[] tempvalues = new byte[bytes];

            //锁定内存区域
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            //平移处理
            if (bmpdata.Stride / bmpdata.Width != 3)
            {
                curBitmap.UnlockBits(bmpdata);
                MessageBox.Show("图像不是 24位真彩色，操作无法进行！");
                return;
            }
            for (int i = 0; i < bmpdata.Height; i++)
            {
                for (int j = 0; j < bmpdata.Width; j++)
                {
                    if (((i + dy) >= 0) && ((i + dy) < bmpdata.Height) && ((j - dx) >= 0) && ((j - dx) < bmpdata.Width))
                    {
                        tempvalues[(bmpdata.Height - 1 - i) * bmpdata.Stride + j * 3 + 2] = rgbValues[(bmpdata.Height - 1 - (i + dy)) * bmpdata.Stride + (j - dx) * 3 + 2];
                        tempvalues[(bmpdata.Height - 1 - i) * bmpdata.Stride + j * 3 + 1] = rgbValues[(bmpdata.Height - 1 - (i + dy)) * bmpdata.Stride + (j - dx) * 3 + 1];
                        tempvalues[(bmpdata.Height - 1 - i) * bmpdata.Stride + j * 3] = rgbValues[(bmpdata.Height - 1 - (i + dy)) * bmpdata.Stride + (j - dx) * 3];
                    }
                    else
                    {
                        tempvalues[(bmpdata.Height - 1 - i) * bmpdata.Stride + j * 3 + 2] = 255;
                        tempvalues[(bmpdata.Height - 1 - i) * bmpdata.Stride + j * 3 + 1] = 255;
                        tempvalues[(bmpdata.Height - 1 - i) * bmpdata.Stride + j * 3] = 255;
                    }
                }
            }
            //将缓冲空间中数据复制回图像数据区域中
            System.Runtime.InteropServices.Marshal.Copy(tempvalues, 0, ptr, bytes);

            //解开内存锁定
            bitmap.UnlockBits(bmpdata);
        }

        private void IDM_IMGVMORO_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                ImageVMoro(curBitmap);
                Invalidate();
            }
        }

        public void ImageVMoro(Bitmap bitmap)
        {
            //创建位图矩形区域 
            Rectangle rect = new Rectangle(0, 0, bitmap.Width, curBitmap.Height);

            //锁定图像数据区域
            System.Drawing.Imaging.BitmapData bmpdata = bitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, curBitmap.PixelFormat);

            //获取独享数据区域的首地址
            IntPtr ptr = bmpdata.Scan0;

            //计算机图像数据占用的内存大小，病分配数据处理缓冲空间
            int bytes = bmpdata.Stride * bmpdata.Height;
            byte[] rgbValues = new byte[bytes];
            byte[] tempvalues = new byte[bytes];

            //锁定内存区域
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            //灰度变换
            if (bmpdata.Stride / bmpdata.Width != 3)
            {
                curBitmap.UnlockBits(bmpdata);
                MessageBox.Show("图像不是 24位真彩色，操作无法进行！");
                return;
            }
            for (int i = 0; i < bmpdata.Height; i++)
            {
                for (int j = 0; j < bmpdata.Width; j++)
                {
                    tempvalues[i * bmpdata.Stride + j * 3 + 2] = rgbValues[i * bmpdata.Stride + (bmpdata.Width - 1 - j) * 3 + 2];
                    tempvalues[i * bmpdata.Stride + j * 3 + 1] = rgbValues[i * bmpdata.Stride + (bmpdata.Width - 1 - j) * 3 + 1];
                    tempvalues[i * bmpdata.Stride + j * 3] = rgbValues[i * bmpdata.Stride + (bmpdata.Width - 1 - j) * 3];
                }
            }
            //将缓冲空间中数据复制回图像数据区域中
            System.Runtime.InteropServices.Marshal.Copy(tempvalues, 0, ptr, bytes);

            //解开内存锁定
            bitmap.UnlockBits(bmpdata);
            //MessageBox.Show("");
        }




        private void IDM_IMGHMORO_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                IamgeHMoro(curBitmap);
            }
            Invalidate();
        }

        public void IamgeHMoro(Bitmap bitmap)
        {
            //创建位图矩形区域 
            Rectangle rect = new Rectangle(0, 0, bitmap.Width, curBitmap.Height);

            //锁定图像数据区域
            System.Drawing.Imaging.BitmapData bmpdata = bitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, bitmap.PixelFormat);

            //获取独享数据区域的首地址
            IntPtr ptr = bmpdata.Scan0;

            //计算机图像数据占用的内存大小，病分配数据处理缓冲空间
            int bytes = bmpdata.Stride * bmpdata.Height;
            byte[] rgbValues = new byte[bytes];
            byte[] tempvalues = new byte[bytes];

            //锁定内存区域
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            //灰度变换
            if (bmpdata.Stride / bmpdata.Width != 3)
            {
                curBitmap.UnlockBits(bmpdata);
                MessageBox.Show("图像不是 24位真彩色，操作无法进行！");
                return;
            }
            for (int i = 0; i < bmpdata.Height; i++)
            {
                for (int j = 0; j < bmpdata.Width; j++)
                {
                    tempvalues[i * bmpdata.Stride + j * 3 + 2] = rgbValues[(bmpdata.Height - 1 - i) * bmpdata.Stride + j * 3 + 2];
                    tempvalues[i * bmpdata.Stride + j * 3 + 1] = rgbValues[(bmpdata.Height - 1 - i) * bmpdata.Stride + j * 3 + 1];
                    tempvalues[i * bmpdata.Stride + j * 3] = rgbValues[(bmpdata.Height - 1 - i) * bmpdata.Stride + j * 3];
                }
            }
            //将缓冲空间中数据复制回图像数据区域中
            System.Runtime.InteropServices.Marshal.Copy(tempvalues, 0, ptr, bytes);

            //解开内存锁定
            bitmap.UnlockBits(bmpdata);
            //MessageBox.Show("");
        }

        public void ImageTrance(Bitmap bitmap)
        {
            //创建位图矩形区域 
            Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);

            //锁定图像数据区域
            System.Drawing.Imaging.BitmapData bmpdata = bitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, bitmap.PixelFormat);

            //获取独享数据区域的首地址
            IntPtr ptr = bmpdata.Scan0;

            //计算机图像数据占用的内存大小，病分配数据处理缓冲空间
            int bytes = bmpdata.Stride * bmpdata.Height;
            byte[] rgbValues = new byte[bytes];
            int newStride = bmpdata.Height + (4 - (bmpdata.Height % 4));
            int newH = bmpdata.Stride;
            int newW = bmpdata.Height;
            int newbytes = newStride * newH;
            byte[] tempvalues = new byte[newbytes];
            //MessageBox.Show((bmpdata.Stride - bmpdata.Width*3).ToString());
            //锁定内存区域
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            //灰度变换
            if (bmpdata.Stride / bmpdata.Width != 3)
            {
                curBitmap.UnlockBits(bmpdata);
                MessageBox.Show("图像不是 24位真彩色，操作无法进行！");
                return;
            }
            for (int i = 0; i < bmpdata.Height; i++)
            {
                for (int j = 0; j < bmpdata.Width; j++)
                {
                    tempvalues[j * newStride * 3 + i * 3 + 2] = rgbValues[i * bmpdata.Stride * 3 + j * 3 + 2];
                    tempvalues[j * newStride * 3 + i * 3 + 1] = rgbValues[i * bmpdata.Stride * 3 + j * 3 + 1];
                    tempvalues[j * newStride * 3 + i * 3] = rgbValues[i * bmpdata.Stride * 3 + j * 3];
                }
            }

            //将缓冲空间中数据复制回图像数据区域中
            System.Runtime.InteropServices.Marshal.Copy(tempvalues, 0, ptr, bytes);

            //解开内存锁定
            bitmap.UnlockBits(bmpdata);
            // Image im = bitmap;
            // bitmap= new Bitmap(im,200,400);

            // MessageBox.Show("");
        }


        public Bitmap subImage(Bitmap bitmap, int x, int y, int w, int h)
        {
            Bitmap b = new Bitmap(w, h, bitmap.PixelFormat);
            //创建位图矩形区域 
            Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            //创建位图矩形区域 
            Rectangle rectb = new Rectangle(0, 0, b.Width, b.Height);
            //锁定图像数据区域
            System.Drawing.Imaging.BitmapData bmpdata = bitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, bitmap.PixelFormat);
            System.Drawing.Imaging.BitmapData bmpdatab = b.LockBits(rectb, System.Drawing.Imaging.ImageLockMode.ReadWrite, b.PixelFormat);
            //获取独享数据区域的首地址
            IntPtr ptr = bmpdata.Scan0;
            IntPtr ptrb = bmpdatab.Scan0;

            //计算机图像数据占用的内存大小，病分配数据处理缓冲空间
            int bytes = bmpdata.Stride * bmpdata.Height;
            byte[] rgbValues = new byte[bytes];

            int bytesb = bmpdatab.Stride * bmpdatab.Height;
            byte[] rgbValuesb = new byte[bytesb];

            // MessageBox.Show(bytes.ToString() + "   " + bytesb.ToString());

            //读取数据
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
            System.Runtime.InteropServices.Marshal.Copy(ptrb, rgbValuesb, 0, bytesb);

            //复制图像数据
            if ((x + w) > bmpdata.Width && (y + h) > bmpdata.Height)
            {
                MessageBox.Show("数据越界");
            }
            for (int i = 0; i < bmpdatab.Height; i++)
            {
                for (int j = 0; j < bmpdatab.Width; j++)
                {
                    try
                    {
                        rgbValuesb[i * bmpdatab.Stride + j * 3 + 2] = rgbValues[(i + y) * bmpdata.Stride + (j + x) * 3 + 2];
                        rgbValuesb[i * bmpdatab.Stride + j * 3 + 1] = rgbValues[(i + y) * bmpdata.Stride + (j + x) * 3 + 1];
                        rgbValuesb[i * bmpdatab.Stride + j * 3] = rgbValues[(i + y) * bmpdata.Stride + (j + x) * 3];

                        // rgbValuesb[i * bmpdatab.Stride  + j  + 2] = 255;
                        // rgbValuesb[i * bmpdatab.Stride + j + 1] = 0;
                        // rgbValuesb[i * bmpdatab.Stride + j ] = 0;
                    }
                    catch (Exception exc)
                    {
                        String str = "";
                        str = "H=" + bmpdatab.Height.ToString() + "  W=" + bmpdatab.Width.ToString() + "\n";
                        str += "i=" + i.ToString() + ", j=" + j.ToString() + ",每行：" + bmpdatab.Stride.ToString() + ", 总坐标" + ((i * bmpdatab.Stride + j) * 3 + 2).ToString() + "\n";
                        //str+=(((i * bmpdata.Stride+ j+x)*3  + 2).ToString()+"  "+bytes.ToString()+"   "+bytesb.ToString();
                        MessageBox.Show(str);
                        bitmap.UnlockBits(bmpdata);
                        b.UnlockBits(bmpdatab);
                        return b;
                    }
                }
            }
            //将缓冲空间中数据复制回图像数据区域中
            System.Runtime.InteropServices.Marshal.Copy(rgbValuesb, 0, ptrb, bytesb);

            //解开内存锁定
            bitmap.UnlockBits(bmpdata);
            b.UnlockBits(bmpdatab);
            //MessageBox.Show(bmpdatab.Stride.ToString());

            return b;
        }


        /*提取子图*/
        /*提取子图*/
        /*提取子图*/
        /*提取子图*/

        private void 提取子图ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SubImpSetDlg dlg = new SubImpSetDlg();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                int x = dlg.X;
                int y = dlg.Y;
                int w = dlg.W;
                int h = dlg.H;
                pictureBox2.Image = subImage(curBitmap, x, y, w, h);
            }
            Invalidate();
            // MessageBox.Show(b.Width.ToString() + "  " + b.Height.ToString());
        }

        /*灰度变化*/
        /*灰度变化*/
        /*灰度变化*/
        /*灰度变化*/

        private void IDM_ToGrayGDI_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                Color curcolor;
                int ret;
                for (int i = 0; i < curBitmap.Width; i++)
                {
                    for (int j = 0; j < curBitmap.Height; j++)
                    {
                        curcolor = curBitmap.GetPixel(i, j);
                        ret = (int)(curcolor.R * 0.299 + curcolor.G * 0.578 + curcolor.B * 0.114);
                        curBitmap.SetPixel(i, j, Color.FromArgb(ret, ret, ret));
                    }
                }
            }
            //刷新窗口，重新绘制，执行paint()方法
            Invalidate();
        }

        /*真正灰度变化*/
        /*真正灰度变化*/
        /*真正灰度变化*/
        /*真正灰度变化*/
        private void IDM_ToGrayMemery_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                //创建位图矩形区域 
                Rectangle rect = new Rectangle(0, 0, curBitmap.Width, curBitmap.Height);

                //锁定图像数据区域
                System.Drawing.Imaging.BitmapData bmpdata = curBitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, curBitmap.PixelFormat);

                //获取独享数据区域的首地址
                IntPtr ptr = bmpdata.Scan0;

                //计算机图像数据占用的内存大小，病分配数据处理缓冲空间
                int bytes = bmpdata.Stride * bmpdata.Height;
                byte[] rgbValues = new byte[bytes];

                //锁定内存区域
                System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

                //灰度变换
                if (bmpdata.Stride / bmpdata.Width != 3)
                {
                    curBitmap.UnlockBits(bmpdata);
                    MessageBox.Show("图像不是 24位真彩色，操作无法进行！");
                    return;
                }
                double curcolor = 0;
                for (int i = 0; i < bmpdata.Height; i++)
                {
                    for (int j = 0; j < bmpdata.Width * 3; j += 3)
                    {
                        curcolor = rgbValues[i * bmpdata.Stride + j + 2] * 0.299 + rgbValues[i * bmpdata.Stride + j + 1] * 0.587 + rgbValues[i * bmpdata.Stride + j] * 0.114;
                        rgbValues[i * bmpdata.Stride + j + 2] = (byte)curcolor;
                        rgbValues[i * bmpdata.Stride + j + 1] = (byte)curcolor;
                        rgbValues[i * bmpdata.Stride + j] = (byte)curcolor;
                    }
                }
                //将缓冲空间中数据复制回图像数据区域中
                System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);

                //解开内存锁定
                curBitmap.UnlockBits(bmpdata);
                //MessageBox.Show(bmpdata.Stride.ToString()+"  "+curBitmap.Width.ToString());
                Invalidate();
            }
        }

        private void IDM_ToGrayPtr_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                //创建位图矩形区域 
                Rectangle rect = new Rectangle(0, 0, curBitmap.Width, curBitmap.Height);

                //锁定图像数据区域
                System.Drawing.Imaging.BitmapData bmpdata = curBitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, curBitmap.PixelFormat);

                byte temp = 0;

                if (bmpdata.Stride / bmpdata.Width != 3)
                {
                    curBitmap.UnlockBits(bmpdata);
                    MessageBox.Show("图像不是 24位真彩色，操作无法进行！");
                    return;
                }

                unsafe
                {
                    byte* ptr = (byte*)(bmpdata.Scan0);
                    for (int i = 0; i < bmpdata.Height; i++)
                    {
                        for (int j = 0; j < bmpdata.Width; j++)
                        {
                            temp = (byte)(ptr[2] * 0.299 + ptr[1] * 0.587 + ptr[0] * 0.114);
                            ptr[2] = ptr[1] = ptr[0] = temp;
                            ptr += 3;
                        }
                        ptr += bmpdata.Stride - bmpdata.Width * 3;

                    }
                }


                //解开内存锁定
                curBitmap.UnlockBits(bmpdata);
                //MessageBox.Show(Convert.ToString(bmpdata.Stride - bmpdata.Width * 3) + "," + Convert.ToString(bmpdata.Stride /bmpdata.Width));
                //MessageBox.Show(bmpdata.Stride.ToString()+"  "+curBitmap.Width.ToString());
                Invalidate();
            }
        }

        private void 小波变化ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 灰度ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                Rectangle rect = new Rectangle(0, 0, curBitmap.Width, curBitmap.Height);
                System.Drawing.Imaging.BitmapData bmpdata = curBitmap.LockBits(rect,
                    System.Drawing.Imaging.ImageLockMode.ReadWrite, curBitmap.PixelFormat);
                IntPtr ptr = bmpdata.Scan0;
                int bytes = bmpdata.Stride * bmpdata.Height;
                Byte[] rgbValue = new Byte[bytes];
                System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValue, 0, bytes);
                if (bmpdata.Stride / bmpdata.Width != 3)
                {
                    MessageBox.Show("图像不是24位真彩");
                    return;

                }
                double curcolor = 0;
                for (int i = 0; i < bmpdata.Height; i++)
                {
                    for (int j = 0; j < bmpdata.Width * 3; j += 3)
                    {
                        curcolor = rgbValue[i * bmpdata.Stride + j + 2] * 0.299 +
                                 rgbValue[i * bmpdata.Stride + j + 1] * 0.587 +
                                 rgbValue[i * bmpdata.Stride + j] * 0.114;
                        rgbValue[i * bmpdata.Stride + j + 2] = Convert.ToByte(curcolor);
                        rgbValue[i * bmpdata.Stride + j + 1] = Convert.ToByte(curcolor);
                        rgbValue[i * bmpdata.Stride + j + 0] = Convert.ToByte(curcolor);
                    }
                }
                System.Runtime.InteropServices.Marshal.Copy(rgbValue, 0, ptr, bytes);
                curBitmap.UnlockBits(bmpdata);
                //panel1.InValidate();
            }
            Invalidate();
        }

        private void 哈尔小波ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                HaarWaveleteHTrance(curBitmap);
                Invalidate();
            }
        }

        public static void HaarWaveleteHTrance(Bitmap bitmap)
        {

            // Bitmap b = new Bitmap(1, 1);
            if (bitmap != null)
            {
                //创建位图矩形区域 
                Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);

                //锁定图像数据区域
                System.Drawing.Imaging.BitmapData bmpdata = bitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, bitmap.PixelFormat);

                //获取图像数据区域的首地址
                IntPtr ptr = bmpdata.Scan0;

                //计算机图像数据占用的内存大小，并分配数据处理缓冲空间
                int bytes = bmpdata.Stride * bmpdata.Height;
                byte[] temp1 = new byte[bytes];
                byte[] bitmapdata = new byte[bytes];

                //锁定内存区域
                System.Runtime.InteropServices.Marshal.Copy(ptr, bitmapdata, 0, bytes);

                //将源图像数据bitmapdata按偶数行和奇数行分别存入temp1
                int height, width, nheight;
                height = bmpdata.Height;
                width = bmpdata.Width;
                nheight = height / 2;
                int pixformat = bmpdata.Stride / width;

                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < nheight; j++)
                    {
                        int h = j * 2;
                        for (int f = 0; f < pixformat; f++)
                        {
                            //偶数行
                            temp1[j * bmpdata.Stride + i * pixformat + f] = bitmapdata[h * bmpdata.Stride + i * pixformat + f];
                            //奇数行
                            temp1[(nheight + j) * bmpdata.Stride + i * pixformat + f] = bitmapdata[bmpdata.Stride * (h + 1) + i * pixformat + f];
                        }
                    }
                }

                //数据的小波差分变换
                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < nheight - 1; j++)
                    {
                        for (int f = 0; f < pixformat; f++)
                        {
                            temp1[(nheight + j) * bmpdata.Stride + i * pixformat + f] = (byte)(temp1[(nheight + j) * bmpdata.Stride + i * pixformat + f] - temp1[j * bmpdata.Stride + i * pixformat + f] + 128);

                        }
                    }
                }

                //建立显示图像
                ///Bitmap b = new Bitmap(width, height, bitmap.PixelFormat);
                //锁定图像数据区域
                ///System.Drawing.Imaging.BitmapData bdata = b.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, b.PixelFormat);
                ///IntPtr ptrb = bdata.Scan0;
                System.Runtime.InteropServices.Marshal.Copy(temp1, 0, ptr, bytes);

                //解开内存锁定
                bitmap.UnlockBits(bmpdata);
                ///b.UnlockBits(bdata);
                // MessageBox.Show("已经调用");
            }
            ///return b;

        }
        //从这里开始，调用
        private void 绘画直方图ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                HistView_Paint(this.pictureBox2.CreateGraphics());
            }
        }
        //绘画函数
        private void HistView_Paint(Graphics g)
        {
            int[] graylevel = new int[257];
            for (int i = 0; i < 256; i++)
            {
                graylevel[i] = 0;
            }
            int maxpixel = 0;   //最大像素个数

            Rectangle rect = new Rectangle(0, 0, curBitmap.Width, curBitmap.Height);
            BitmapData bmpData = curBitmap.LockBits(rect, ImageLockMode.ReadOnly, curBitmap.PixelFormat);
            int width = bmpData.Width;
            int height = bmpData.Height;
            int stride = bmpData.Stride; // 扫描线的宽度 
            IntPtr ptr = bmpData.Scan0; // 获取bmpData的内存起始位置 
            int scanBytes = width * height; // 用stride宽度，表示这是内存区域的大小 
            byte[] rgbValues = new byte[scanBytes];
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, scanBytes); // 将图像数据拷贝到rgbValues中 

            for (int i = 0; i < scanBytes; i++)
            {
                byte temp = rgbValues[i];
                graylevel[temp]++;

                if (graylevel[temp] > maxpixel)
                {
                    maxpixel = graylevel[temp];
                }
            }
            //graylevel[256] = maxpixel;
            curBitmap.UnlockBits(bmpData);
            //return graylevel;
            //Graphics g = e.Graphics;
            Pen curPen = new Pen(Brushes.Black, 1);
            //Graphics g = this.CreateGraphics();

            g.DrawLine(curPen, 50, 240, 320, 240);
            g.DrawLine(curPen, 50, 240, 50, 30);
            g.DrawLine(curPen, 50, 240, 320, 240);
            g.DrawLine(curPen, 100, 240, 100, 242);
            g.DrawLine(curPen, 150, 240, 150, 242);
            g.DrawLine(curPen, 200, 240, 200, 242);
            g.DrawLine(curPen, 250, 240, 250, 242);
            g.DrawLine(curPen, 300, 240, 300, 242);
            g.DrawString("0", new Font("new Timer", 8), Brushes.Black, new Point(46, 242));
            g.DrawString("50", new Font("new Timer", 8), Brushes.Black, new Point(92, 242));
            g.DrawString("100", new Font("new Timer", 8), Brushes.Black, new Point(139, 242));
            g.DrawString("150", new Font("new Timer", 8), Brushes.Black, new Point(189, 242));
            g.DrawString("200", new Font("new Timer", 8), Brushes.Black, new Point(239, 242));
            g.DrawString("250", new Font("new Timer", 8), Brushes.Black, new Point(289, 242));
            g.DrawLine(curPen, 48, 40, 50, 40);
            g.DrawString("0", new Font("new Timer", 8), Brushes.Black, new Point(34, 234));
            g.DrawString(maxpixel.ToString(), new Font("new Timer", 8), Brushes.Black, new Point(18, 34));
            // MessageBox.Show("1111111111111");
            for (int i = 0; i < 256; i++)
            {
                double temp = 200.0 * graylevel[i] / maxpixel;
                g.DrawLine(curPen, 50 + i, 240, 50 + i, 240 - (int)temp);
            }
            curPen.Dispose();
        }

        private void 缩小ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            //pictureBox1.Width = Convert.ToInt32(pictureBox1.Width * 0.8);
            // pictureBox1.Height = Convert.ToInt32(pictureBox1.Height * 0.8);
            if (curBitmap != null)
            {
                IamgeLx(curBitmap);
            }
            Invalidate();
        }

        /*
        图片网格
        */

        public void IamgeLx(Bitmap bitmap)
        { //图像变大函数
            Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            //锁定图像数据区域
            System.Drawing.Imaging.BitmapData bmpdata = bitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, curBitmap.PixelFormat);
            //获得独享数据区域首地址
            IntPtr ptr0 = bmpdata.Scan0;
            //计算出图像需要的内存大小，并分配内存
            int byteall = bmpdata.Stride * bmpdata.Height;
            byte[] rgbValue = new byte[byteall];
            byte[] tempValues = new byte[2 * byteall];
            //锁定内存区域
            System.Runtime.InteropServices.Marshal.Copy(ptr0, rgbValue, 0, byteall);
            //开始大小变化 变为原来的2倍
            // int j = byteall;
            for (int i = 0; i < byteall; i++)
            {
                if (i % 2 == 0)
                {
                    tempValues[i] = rgbValue[i];

                }
            }
            System.Runtime.InteropServices.Marshal.Copy(tempValues, 0, ptr0, byteall);
            bitmap.UnlockBits(bmpdata);

        }
        /*
         图片分割
         */
        private void 图片分割ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                int[] graylevel = new int[257];
                for (int i = 0; i < 256; i++)
                {
                    graylevel[i] = 0;
                }
                int maxpixel = 0;   //最大像素个数
                int maxpixel_2 = 0;
                int maxpixel_3 = 0;

                Rectangle rect = new Rectangle(0, 0, curBitmap.Width, curBitmap.Height);
                BitmapData bmpData = curBitmap.LockBits(rect, ImageLockMode.ReadOnly, curBitmap.PixelFormat);
                int width = bmpData.Width;
                int height = bmpData.Height;
                int stride = bmpData.Stride; // 扫描线的宽度 
                IntPtr ptr = bmpData.Scan0; // 获取bmpData的内存起始位置 
                int scanBytes = width * height * 3; // 用stride宽度，表示这是内存区域的大小 
                byte[] rgbValues = new byte[scanBytes];
                byte[] pbox1 = new byte[scanBytes];
                // byte[] pbox2 = new byte[scanBytes];
                System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, scanBytes); // 将图像数据拷贝到rgbValues中 

                for (int i = 0; i < scanBytes; i++)
                {
                    byte temp = rgbValues[i];
                    graylevel[temp]++;

                    if (graylevel[temp] > maxpixel)
                    {
                        maxpixel = graylevel[temp];
                    }
                    else if (graylevel[temp] > maxpixel_2)
                    {
                        maxpixel_2 = graylevel[temp];
                    }
                    else if (graylevel[temp] > maxpixel_3)
                    {
                        maxpixel_2 = graylevel[temp];
                    }
                    // Console.Write(graylevel[temp]);
                }
                //MessageBox.Show("41346316");
                for (int j = 0; j < scanBytes; j++)
                {
                    byte tep = rgbValues[j];
                    if (graylevel[tep] != maxpixel && graylevel[tep] != maxpixel_2 && graylevel[tep] != maxpixel_3)
                    {
                        pbox1[j] = rgbValues[j];
                    }
                    //else { pbox1[j] = rgbValues[0]; }
                }
                System.Runtime.InteropServices.Marshal.Copy(pbox1, 0, ptr, scanBytes);
                curBitmap.UnlockBits(bmpData);
                for (int i = 0; i < 10; i++)
                {
                    Invalidate();
                }
            }
        }
        /*
         * 
         * 浮雕算法
         * 
         * 
         * */
        private void 缩小ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {

                F_paint(this.pictureBox2.CreateGraphics());
            }
        }

        public void F_paint(Graphics g)
        {

            g.Clear(Color.White);
            g.ScaleTransform(0.7f, 0.7f);
            int width = curBitmap.Width;
            int height = curBitmap.Height;
            Bitmap image = curBitmap.Clone(new Rectangle(0, 0, width, height), PixelFormat.DontCare);//复制新的内存空间
            g.DrawImage(curBitmap, new Rectangle(0, 0, width, height));//画原图
            Color color, colortemp, colorLeft;
            //依次访问每个像素的RGB值
            for (int i = width - 1; i > 0; i--)
            {
                for (int j = height - 1; j > 0; j--)
                {

                    color = curBitmap.GetPixel(i, j);
                    colorLeft = curBitmap.GetPixel(i - 1, j - 1);
                    //对像素的RGB值处理
                    int R = Math.Max(67, Math.Min(255, Math.Abs(color.R - colorLeft.R + 128)));
                    int G = Math.Max(67, Math.Min(255, Math.Abs(color.G - colorLeft.G + 128)));
                    int B = Math.Max(67, Math.Min(255, Math.Abs(color.B - colorLeft.B + 128)));
                    colortemp = Color.FromArgb(255, R, G, B);
                    image.SetPixel(i, j, colortemp);
                }
            }
            g.DrawImage(image, new Rectangle(0, 0, width, height));
        }

        private void 底片ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                back(this.pictureBox2.CreateGraphics());

            }

        }

        public void back(Graphics g)
        {
            g.Clear(Color.White);
            // g.ScaleTransform(0.7f, 0.7f);
            int width = curBitmap.Width;
            int height = curBitmap.Height;
            Bitmap image = curBitmap.Clone(new Rectangle(0, 0, width, height), PixelFormat.DontCare);//复制新的内存空间
            g.DrawImage(curBitmap, new Rectangle(0, 0, width, height));//画原图
            Color color, colortemp;
            //依次访问每个像素的RGB值
            for (int i = width - 1; i > 0; i--)
            {
                for (int j = height - 1; j > 0; j--)
                {

                    color = curBitmap.GetPixel(i, j);
                    //colorLeft = curBitmap.GetPixel(i - 1, j - 1);
                    //对像素的RGB值处理
                    int R = 255 - color.R;
                    int G = 255 - color.G;
                    int B = 255 - color.B;
                    colortemp = Color.FromArgb(255, R, G, B);
                    image.SetPixel(i, j, colortemp);
                }
            }
            g.DrawImage(image, new Rectangle(0, 0, width, height));
        }
        /*
         * 
         * 马赛克算法
         * */
        private void 原色ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mk(this.pictureBox2.CreateGraphics());
        }
        public void Mk(Graphics g)
        {
            // g.Clear(Color.White);
            // g.ScaleTransform(0.7f, 0.7f);
            int width = curBitmap.Width;
            int height = curBitmap.Height;
            // Console.Write(width * height);
            //int[] lac = {-1,-1,-1,-1,9,-1,-1,-1,-1};
            Bitmap image = curBitmap.Clone(new Rectangle(0, 0, width, height), PixelFormat.DontCare);//复制新的内存空间
            // Bitmap Bmp = (Bitmap)curBitmap.Imaging;
            // g.DrawImage(curBitmap, new Rectangle(0, 0, width, height));//画原图
            Color colortemp;
            //依次访问每个像素的RGB值
            for (int i = 2; i < width - 2; i += 5)
            {
                for (int j = 2; j < height - 2; j += 5)
                {
                    int tempr = 0, tempg = 0, tempb = 0, index = 0;
                    for (int n = -2; n <= 2; n++)
                    {
                        for (int m = -2; m <= 2; m++)
                        {
                            //if(){}
                            Color tempcolor = image.GetPixel(i + n, j + m);
                            tempr += tempcolor.R;
                            tempg += tempcolor.G;
                            tempb += tempcolor.B;
                            index++;

                        }
                    }
                    /* tempr = tempr > 255 ? 255 : tempr;
                     tempr = tempr < 0 ? 0 : tempr;
                     tempg = tempg > 255 ? 255 : tempg;
                     tempg = tempg < 0 ? 0 : tempg;
                     tempb = tempb > 255 ? 255 : tempg;
                     tempb = tempb < 0 ? 0 : tempb;
                     color = curBitmap.GetPixel(i, j);
                     //colorLeft = curBitmap.GetPixel(i - 1, j - 1);
                     //对像素的RGB值处理
                     int R = 255 - color.R;
                     int G = 255 - color.G;
                     int B = 255 - color.B;*/
                    tempr = tempr / 25;
                    tempg = tempg / 25;
                    tempb = tempb / 25;
                    colortemp = Color.FromArgb(255, tempr, tempg, tempb);
                    for (int n = -2; n <= 2; n++)
                    {
                        for (int m = -2; m <= 2; m++)
                        {
                            image.SetPixel(i + n, j + m, colortemp);
                        }
                    }
                }
            }
            g.DrawImage(image, new Rectangle(0, 0, width, height));

        }
        /*
         * 漫画
         * 
         * */
        private void 马赛克ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mh(this.pictureBox2.CreateGraphics());
        }
        public void Mh(Graphics g)
        {
            int width = curBitmap.Width;
            int height = curBitmap.Height;
            Bitmap image = curBitmap.Clone(new Rectangle(0, 0, width, height), PixelFormat.DontCare);
            Color indexcolor;
            for (int i = 1; i <= width - 1; i += 2)
            {
                for (int j = 1; j <= height - 1; j += 2)
                {
                    int n = 0, m = 0;
                    Random rd = new Random();
                    n = rd.Next(-1, 1);
                    m = rd.Next(-1, 1);
                    indexcolor = image.GetPixel(i + n, j + m);
                    image.SetPixel(i, j, indexcolor);
                }

            }
            g.DrawImage(image, new Rectangle(0, 0, width, height));
        }
        /*
         * 
         * 锐化
         * */
        private void 锐化ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Rh(this.pictureBox2.CreateGraphics());
        }
        public void Rh(Graphics g)
        {
            g.Clear(Color.White);
            int width = curBitmap.Width;
            int height = curBitmap.Height;
            int[] tm = { 1, 1, 1, 1, 2, 1, 1, 1, 1 };
            Bitmap image = curBitmap.Clone(new Rectangle(0, 0, width, height), PixelFormat.DontCare);
            Color tempcolor;
            for (int i = 1; i < width - 1; i++)
            {
                for (int j = 1; j < height - 1; j++)
                {
                    int tempr = 0, tempg = 0, tempb = 0, index = 0;
                    for (int n = -1; n <= 1; n++)
                    {
                        for (int m = -1; m <= 1; m++)
                        {
                            tempcolor = image.GetPixel(i + n, j + m);
                            tempr = tempcolor.R * tm[index];
                            tempg = tempcolor.G * tm[index];
                            tempb = tempcolor.B * tm[index];
                            index++;

                            tempr = tempr > 255 ? 255 : tempr;
                            tempr = tempr < 0 ? 0 : tempr;
                            tempg = tempg > 255 ? 255 : tempg;
                            tempg = tempg < 0 ? 0 : tempg;
                            tempb = tempb > 255 ? 255 : tempg;
                            tempb = tempb < 0 ? 0 : tempb;
                            Color indexcolor = Color.FromArgb(tempr, tempg, tempb);
                            image.SetPixel(i + n, j + m, indexcolor);
                        }
                    }
                }
            }
            g.DrawImage(image, new Rectangle(0, 0, width, height));
        }

        /* private void 平移ToolStripMenuItem_Click(object sender, EventArgs e)
         {
             Py(this.pictureBox1.CreateGraphics());
             Invalidate();
         }
         public void Py(Graphics g) {
             int width = curBitmap.Width;
             int height = curBitmap.Height;
             Bitmap image = curBitmap.Clone(new Rectangle(0, 0, curBitmap.Width, curBitmap.Height), PixelFormat.DontCare);
             pictureBox1.Image = image;
            
             g.DrawImage(curBitmap,new Rectangle(100,100,width,height));
             pictureBox1.Width = (Int32)( width + 100);
             pictureBox1.Height = (Int32)(height + 100);
         
         }*/

        /*
         * 
         * 平滑
         * */

        public void Ph(Graphics g)
        {
            g.Clear(Color.White);
            int width = curBitmap.Width;
            int height = curBitmap.Height;
            int[] tm = { 1, 2, 1, 2, 4, 2, 1, 2, 1 };
            Bitmap image = curBitmap.Clone(new Rectangle(0, 0, width, height), PixelFormat.DontCare);
            Color tempcolor;
            for (int i = 1; i < width - 1; i++)
            {
                for (int j = 1; j < height - 1; j++)
                {
                    int tempr = 0, tempg = 0, tempb = 0, index = 0;
                    for (int n = -1; n <= 1; n++)
                    {
                        for (int m = -1; m <= 1; m++)
                        {
                            tempcolor = image.GetPixel(i + n, j + m);
                            tempr = tempcolor.R * tm[index];
                            tempg = tempcolor.G * tm[index];
                            tempb = tempcolor.B * tm[index];
                            index++;

                            tempr = tempr > 255 ? 255 : tempr;
                            tempr = tempr < 0 ? 0 : tempr;
                            tempg = tempg > 255 ? 255 : tempg;
                            tempg = tempg < 0 ? 0 : tempg;
                            tempb = tempb > 255 ? 255 : tempg;
                            tempb = tempb < 0 ? 0 : tempb;
                            Color indexcolor = Color.FromArgb(tempr, tempg, tempb);
                            image.SetPixel(i + n, j + m, indexcolor);
                        }
                    }
                }
            }
            g.DrawImage(image, new Rectangle(0, 0, width, height));
        }


        private void 平移ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ph(this.pictureBox2.CreateGraphics());
        }

        private void 原色ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                back(this.pictureBox2.CreateGraphics());

            }
        }

        private void 彩色ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cs(this.pictureBox2.CreateGraphics());
        }
        public void Cs(Graphics g)
        {
            g.Clear(Color.White);
            // g.ScaleTransform(0.7f, 0.7f);
            int width = curBitmap.Width;
            int height = curBitmap.Height;
            Bitmap image = curBitmap.Clone(new Rectangle(0, 0, width, height), PixelFormat.DontCare);//复制新的内存空间
            g.DrawImage(curBitmap, new Rectangle(0, 0, width, height));//画原图
            Color color, colortemp;
            //依次访问每个像素的RGB值
            for (int i = width - 1; i > 0; i--)
            {
                for (int j = height - 1; j > 0; j--)
                {

                    color = curBitmap.GetPixel(i, j);
                    //colorLeft = curBitmap.GetPixel(i - 1, j - 1);
                    //对像素的RGB值处理
                    int R = 255 - color.R;
                    int G = 255 - color.G;
                    int B = 255 - color.B;
                    R = (int)((R - G * 0.578 - B * 0.114) / 0.299);
                    G = (int)((G - R * 0.299 - B * 0.114) / 0.578);
                    B = (int)((B - R * 0.299 - G * 0.578) / 0.114);
                    if (R > 255) { R = 255; }
                    if (R < 0) { R = 0; }
                    if (G > 255) { G = 255; }
                    if (G < 0) { G = 0; }
                    if (B > 255) { B = 255; }
                    if (B < 0) { B = 0; }

                    colortemp = Color.FromArgb(255, R, G, B);
                    image.SetPixel(i, j, colortemp);
                }
            }

            g.DrawImage(image, new Rectangle(0, 0, width, height));


        }

        private void 独自完成ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 二值化ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                byte maxpixel = 0;
                byte minpixel = 0;
                byte T = 0;
                Rectangle rect = new Rectangle(0, 0, curBitmap.Width, curBitmap.Height);
                BitmapData bmpData = curBitmap.LockBits(rect, ImageLockMode.ReadOnly, curBitmap.PixelFormat);
                int width = bmpData.Width;
                int height = bmpData.Height;
                int stride = bmpData.Stride; // 扫描线的宽度 
                IntPtr ptr = bmpData.Scan0; // 获取bmpData的内存起始位置 
                int scanBytes = width * height * 3; // 用stride宽度，表示这是内存区域的大小
                byte[] rgbValues = new byte[scanBytes];
                System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, scanBytes); // 将图像数据拷贝到rgbValues中
                for (int i = 0; i < scanBytes; i++)
                {
                    if (rgbValues[i] > maxpixel)
                    {
                        maxpixel = rgbValues[i];
                    }
                    if (rgbValues[i] < minpixel)
                    {
                        minpixel = rgbValues[i];
                    }

                }
                T = (byte)(maxpixel - ((maxpixel - minpixel) / 3));
                for (int j = 0; j < scanBytes; j++)
                {
                    //rgbValues[j] = rgbValues[j] < T ? 0 : 255;
                    if (rgbValues[j] < T)
                    {
                        rgbValues[j] = 0;
                    }
                    else { rgbValues[j] = 255; }

                }
                System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, scanBytes);
                curBitmap.UnlockBits(bmpData);
                this.pictureBox2.CreateGraphics().DrawImage(curBitmap, 0, 0);

            }
        }

        private void 柔化ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                soften();
            }
        }
        public void soften()
        {
            try
            {
                int Height = this.pictureBox1.Image.Height;
                int Width = this.pictureBox1.Image.Width;
                Bitmap bitmap = new Bitmap(Width, Height);
                Bitmap MyBitmap = (Bitmap)this.pictureBox1.Image;
                Color pixel;
                //高斯模板
                int[] Gauss = { 1, 2, 1, 2, 4, 2, 1, 2, 1 };
                for (int x = 1; x < Width - 1; x++)
                    for (int y = 1; y < Height - 1; y++)
                    {
                        int r = 0, g = 0, b = 0;
                        int Index = 0;
                        for (int col = -1; col <= 1; col++)
                            for (int row = -1; row <= 1; row++)
                            {
                                pixel = MyBitmap.GetPixel(x + row, y + col);
                                r += pixel.R * Gauss[Index];
                                g += pixel.G * Gauss[Index];
                                b += pixel.B * Gauss[Index];
                                Index++;
                            }
                        r /= 16;
                        g /= 16;
                        b /= 16;
                        //处理颜色值溢出
                        r = r > 255 ? 255 : r;
                        r = r < 0 ? 0 : r;
                        g = g > 255 ? 255 : g;
                        g = g < 0 ? 0 : g;
                        b = b > 255 ? 255 : b;
                        b = b < 0 ? 0 : b;
                        bitmap.SetPixel(x - 1, y - 1, Color.FromArgb(r, g, b));
                    }
                this.pictureBox1.Image = bitmap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "信息提示");
            }

        }

        private void 锐化ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                sharpen();
            }
        }
        public void sharpen()
        {
            try
            {
                int Height = this.pictureBox1.Image.Height;
                int Width = this.pictureBox1.Image.Width;
                Bitmap newBitmap = new Bitmap(Width, Height);
                Bitmap oldBitmap = (Bitmap)this.pictureBox1.Image;
                Color pixel;
                //拉普拉斯模板
                int[] Laplacian = { -1, -1, -1, -1, 9, -1, -1, -1, -1 };
                for (int x = 1; x < Width - 1; x++)
                    for (int y = 1; y < Height - 1; y++)
                    {
                        int r = 0, g = 0, b = 0;
                        int Index = 0;
                        for (int col = -1; col <= 1; col++)
                            for (int row = -1; row <= 1; row++)
                            {
                                pixel = oldBitmap.GetPixel(x + row, y + col); r += pixel.R * Laplacian[Index];
                                g += pixel.G * Laplacian[Index];
                                b += pixel.B * Laplacian[Index];
                                Index++;
                            }
                        //处理颜色值溢出
                        r = r > 255 ? 255 : r;
                        r = r < 0 ? 0 : r;
                        g = g > 255 ? 255 : g;
                        g = g < 0 ? 0 : g;
                        b = b > 255 ? 255 : b;
                        b = b < 0 ? 0 : b;
                        newBitmap.SetPixel(x - 1, y - 1, Color.FromArgb(r, g, b));
                    }
                this.pictureBox1.Image = newBitmap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "信息提示");
            }

        }

        private void 雾化ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                atomi();
            }
        }
        public void atomi()
        {
            try
            {
                int Height = this.pictureBox1.Image.Height;
                int Width = this.pictureBox1.Image.Width;
                Bitmap newBitmap = new Bitmap(Width, Height);
                Bitmap oldBitmap = (Bitmap)this.pictureBox1.Image;
                Color pixel;
                for (int x = 1; x < Width - 1; x++)
                    for (int y = 1; y < Height - 1; y++)
                    {
                        System.Random MyRandom = new Random();
                        int k = MyRandom.Next(123456);
                        //像素块大小
                        int dx = x + k % 19;
                        int dy = y + k % 19;
                        if (dx >= Width)
                            dx = Width - 1;
                        if (dy >= Height)
                            dy = Height - 1;
                        pixel = oldBitmap.GetPixel(dx, dy);
                        newBitmap.SetPixel(x, y, pixel);
                    }
                this.pictureBox1.Image = newBitmap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "信息提示");
            }

        }
        //光照效果
        private void 光照ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                lllumination();
            }
        }
        public void lllumination()
        {
            Graphics MyGraphics = this.pictureBox1.CreateGraphics();
            MyGraphics.Clear(Color.White);
            Bitmap MyBmp = new Bitmap(this.pictureBox1.Image, this.pictureBox1.Width, this.pictureBox1.Height);
            int MyWidth = MyBmp.Width;
            int MyHeight = MyBmp.Height;
            Bitmap MyImage = MyBmp.Clone(new RectangleF(0, 0, MyWidth, MyHeight), System.Drawing.Imaging.PixelFormat.DontCare);
            int A = Width / 2;
            int B = Height / 2;
            //MyCenter图片中心点，发亮此值会让强光中心发生偏移
            Point MyCenter = new Point(MyWidth / 2, MyHeight / 2);
            //R强光照射面的半径，即”光晕”
            int R = Math.Min(MyWidth / 2, MyHeight / 2);
            for (int i = MyWidth - 1; i >= 1; i--)
            {
                for (int j = MyHeight - 1; j >= 1; j--)
                {
                    float MyLength = (float)Math.Sqrt(Math.Pow((i - MyCenter.X), 2) + Math.Pow((j - MyCenter.Y), 2));
                    //如果像素位于”光晕”之内
                    if (MyLength < R)
                    {
                        Color MyColor = MyImage.GetPixel(i, j);
                        int r, g, b;
                        //220亮度增加常量，该值越大，光亮度越强
                        float MyPixel = 220.0f * (1.0f - MyLength / R);
                        r = MyColor.R + (int)MyPixel;
                        r = Math.Max(0, Math.Min(r, 255));
                        g = MyColor.G + (int)MyPixel;
                        g = Math.Max(0, Math.Min(g, 255));
                        b = MyColor.B + (int)MyPixel;
                        b = Math.Max(0, Math.Min(b, 255));
                        //将增亮后的像素值回写到位图
                        Color MyNewColor = Color.FromArgb(255, r, g, b);
                        MyImage.SetPixel(i, j, MyNewColor);
                    }
                }
                //重新绘制图片
                MyGraphics.DrawImage(MyImage, new Rectangle(0, 0, MyWidth, MyHeight));
            }
        }

        private void 百叶窗ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                shadesh();
                shadesw();
            }
        }
        public void shadesw()
        {
            try
            {
                Bitmap MyBitmap = (Bitmap)this.pictureBox1.Image.Clone();
                int dw = MyBitmap.Width / 30;
                int dh = MyBitmap.Height;
                Graphics g = this.pictureBox1.CreateGraphics();
                g.Clear(Color.Gray);
                Point[] MyPoint = new Point[30];
                for (int x = 0; x < 30; x++)
                {
                    MyPoint[x].Y = 0;
                    MyPoint[x].X = x * dw;
                }
                Bitmap bitmap = new Bitmap(MyBitmap.Width, MyBitmap.Height);
                for (int i = 0; i < dw; i++)
                {
                    for (int j = 0; j < 30; j++)
                    {
                        for (int k = 0; k < dh; k++)
                        {
                            bitmap.SetPixel(MyPoint[j].X + i, MyPoint[j].Y + k,
                            MyBitmap.GetPixel(MyPoint[j].X + i, MyPoint[j].Y + k));
                        }
                    }
                    this.pictureBox1.Refresh();
                    this.pictureBox1.Image = bitmap;
                    System.Threading.Thread.Sleep(100);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "信息提示");
            }
        }


        public void shadesh()
        {

            //水平百叶窗显示图像
            try
            {
                Bitmap MyBitmap = (Bitmap)this.pictureBox1.Image.Clone();
                int dh = MyBitmap.Height / 20;// 叶数
                int dw = MyBitmap.Width;
                Graphics g = this.pictureBox1.CreateGraphics();
                g.Clear(Color.Gray);
                Point[] MyPoint = new Point[20];
                for (int y = 0; y < 20; y++)//设置叶子起点
                {
                    MyPoint[y].X = 0;
                    MyPoint[y].Y = y * dh;
                }
                Bitmap bitmap = new Bitmap(MyBitmap.Width, MyBitmap.Height);
                for (int i = 0; i < dh; i++)
                {
                    for (int j = 0; j < 20; j++)
                    {
                        for (int k = 0; k < dw; k++)
                        {
                            bitmap.SetPixel(MyPoint[j].X + k, MyPoint[j].Y + i, MyBitmap.GetPixel(MyPoint[j].X + k, MyPoint[j].Y + i));
                        }
                    }
                    this.pictureBox1.Refresh();
                    this.pictureBox1.Image = bitmap;
                    System.Threading.Thread.Sleep(100);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "信息提示");
            }
        }

        private void 油画ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                oil(curBitmap);
            }
        }
        public void oil(Bitmap MyBitmap)
        {
            Graphics g = this.pictureBox1.CreateGraphics();
            //Bitmap bitmap = this.MyBitmap;
            //取得图片尺寸
            int width = MyBitmap.Width;
            int height = MyBitmap.Height;
            RectangleF rect = new RectangleF(0, 0, width, height);
            Bitmap img = MyBitmap.Clone(rect, System.Drawing.Imaging.PixelFormat.DontCare);
            //产生随机数序列
            Random rnd = new Random();
            //取不同的值决定油画效果的不同程度
            int iModel = 2;
            int i = width - iModel;
            while (i > 1)
            {
                int j = height - iModel;
                while (j > 1)
                {
                    int iPos = rnd.Next(100000) % iModel;
                    //将该点的RGB值设置成附近iModel点之内的任一点
                    Color color = img.GetPixel(i + iPos, j + iPos);
                    img.SetPixel(i, j, color);
                    j = j - 1;
                }
                i = i - 1;
            }
            //重新绘制图像
            g.Clear(Color.White);
            g.DrawImage(img, new Rectangle(0, 0, width, height));

        }



        private void 堆积木ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curBitmap != null)
            {
                stack(curBitmap);
            }
        }

        public void stack(Bitmap MyBitmap)
        {
            try
            {
                Graphics myGraphics = this.pictureBox1.CreateGraphics();
                //Bitmap myBitmap = new Bitmap(this.BackgroundImage);
                int myWidth, myHeight, i, j, iAvg, iPixel;
                Color myColor, myNewColor;
                RectangleF myRect;
                myWidth = MyBitmap.Width;
                myHeight = MyBitmap.Height;
                myRect = new RectangleF(0, 0, myWidth, myHeight);
                Bitmap bitmap = MyBitmap.Clone(myRect, System.Drawing.Imaging.PixelFormat.DontCare);
                i = 0;
                while (i < myWidth - 1)
                {
                    j = 0;
                    while (j < myHeight - 1)
                    {
                        myColor = bitmap.GetPixel(i, j);
                        iAvg = (myColor.R + myColor.G + myColor.B) / 3;
                        iPixel = 0;
                        if (iAvg >= 128)
                            iPixel = 255;
                        else
                            iPixel = 0;
                        myNewColor = Color.FromArgb(255, iPixel, iPixel, iPixel);
                        bitmap.SetPixel(i, j, myNewColor);
                        j = j + 1;
                    }
                    i = i + 1;
                }
                myGraphics.Clear(Color.WhiteSmoke);
                myGraphics.DrawImage(bitmap, new Rectangle(0, 0, myWidth, myHeight));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "信息提示");
            }

        }

    }
}
