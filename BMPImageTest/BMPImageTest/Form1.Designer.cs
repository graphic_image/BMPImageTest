﻿namespace BMPImageTest
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.IDM_FILE_OPEN = new System.Windows.Forms.ToolStripMenuItem();
            this.编辑ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.几何变换ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.IDM_IMGMOVE = new System.Windows.Forms.ToolStripMenuItem();
            this.IDM_IMGVMORO = new System.Windows.Forms.ToolStripMenuItem();
            this.IDM_IMGHMORO = new System.Windows.Forms.ToolStripMenuItem();
            this.旋转ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.缩放ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.灰度变换ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.黑白化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.IDM_ToGrayGDI = new System.Windows.Forms.ToolStripMenuItem();
            this.IDM_ToGrayMemery = new System.Windows.Forms.ToolStripMenuItem();
            this.IDM_ToGrayPtr = new System.Windows.Forms.ToolStripMenuItem();
            this.直方图ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.图像增强ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.图像锐化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.图像剪切ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.提取子图ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.频率变化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.小波变化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.独自完成ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.灰度ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.哈尔小波ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.绘画直方图ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.缩小ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.图片分割ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.缩小ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.底片ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.原色ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.马赛克ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.锐化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.平移ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.原色ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.彩色ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.二值化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.增加功能ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.柔化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.锐化ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.雾化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.光照ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.百叶窗ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.油画ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.堆积木ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(5, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(659, 521);
            this.panel1.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(3, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(312, 289);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // 文件ToolStripMenuItem
            // 
            this.文件ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.IDM_FILE_OPEN});
            this.文件ToolStripMenuItem.Name = "文件ToolStripMenuItem";
            this.文件ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.文件ToolStripMenuItem.Text = "文件";
            // 
            // IDM_FILE_OPEN
            // 
            this.IDM_FILE_OPEN.Name = "IDM_FILE_OPEN";
            this.IDM_FILE_OPEN.Size = new System.Drawing.Size(100, 22);
            this.IDM_FILE_OPEN.Text = "打开";
            this.IDM_FILE_OPEN.Click += new System.EventHandler(this.IDM_FILE_OPEN_Click);
            // 
            // 编辑ToolStripMenuItem
            // 
            this.编辑ToolStripMenuItem.Name = "编辑ToolStripMenuItem";
            this.编辑ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.编辑ToolStripMenuItem.Text = "编辑";
            // 
            // 几何变换ToolStripMenuItem
            // 
            this.几何变换ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.IDM_IMGMOVE,
            this.IDM_IMGVMORO,
            this.IDM_IMGHMORO,
            this.旋转ToolStripMenuItem,
            this.缩放ToolStripMenuItem});
            this.几何变换ToolStripMenuItem.Name = "几何变换ToolStripMenuItem";
            this.几何变换ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.几何变换ToolStripMenuItem.Text = "几何变换";
            // 
            // IDM_IMGMOVE
            // 
            this.IDM_IMGMOVE.Name = "IDM_IMGMOVE";
            this.IDM_IMGMOVE.Size = new System.Drawing.Size(124, 22);
            this.IDM_IMGMOVE.Text = "平移";
            this.IDM_IMGMOVE.Click += new System.EventHandler(this.IDM_IMGMOVE_Click);
            // 
            // IDM_IMGVMORO
            // 
            this.IDM_IMGVMORO.Name = "IDM_IMGVMORO";
            this.IDM_IMGVMORO.Size = new System.Drawing.Size(124, 22);
            this.IDM_IMGVMORO.Text = "水平镜像";
            this.IDM_IMGVMORO.Click += new System.EventHandler(this.IDM_IMGVMORO_Click);
            // 
            // IDM_IMGHMORO
            // 
            this.IDM_IMGHMORO.Name = "IDM_IMGHMORO";
            this.IDM_IMGHMORO.Size = new System.Drawing.Size(124, 22);
            this.IDM_IMGHMORO.Text = "垂直镜像";
            this.IDM_IMGHMORO.Click += new System.EventHandler(this.IDM_IMGHMORO_Click);
            // 
            // 旋转ToolStripMenuItem
            // 
            this.旋转ToolStripMenuItem.Name = "旋转ToolStripMenuItem";
            this.旋转ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.旋转ToolStripMenuItem.Text = "旋转";
            // 
            // 缩放ToolStripMenuItem
            // 
            this.缩放ToolStripMenuItem.Name = "缩放ToolStripMenuItem";
            this.缩放ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.缩放ToolStripMenuItem.Text = "缩放";
            // 
            // 灰度变换ToolStripMenuItem
            // 
            this.灰度变换ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.黑白化ToolStripMenuItem,
            this.直方图ToolStripMenuItem,
            this.图像增强ToolStripMenuItem,
            this.图像锐化ToolStripMenuItem});
            this.灰度变换ToolStripMenuItem.Name = "灰度变换ToolStripMenuItem";
            this.灰度变换ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.灰度变换ToolStripMenuItem.Text = "灰度变换";
            // 
            // 黑白化ToolStripMenuItem
            // 
            this.黑白化ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.IDM_ToGrayGDI,
            this.IDM_ToGrayMemery,
            this.IDM_ToGrayPtr});
            this.黑白化ToolStripMenuItem.Name = "黑白化ToolStripMenuItem";
            this.黑白化ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.黑白化ToolStripMenuItem.Text = "黑白化";
            // 
            // IDM_ToGrayGDI
            // 
            this.IDM_ToGrayGDI.Name = "IDM_ToGrayGDI";
            this.IDM_ToGrayGDI.Size = new System.Drawing.Size(119, 22);
            this.IDM_ToGrayGDI.Text = "GDI+法";
            this.IDM_ToGrayGDI.Click += new System.EventHandler(this.IDM_ToGrayGDI_Click);
            // 
            // IDM_ToGrayMemery
            // 
            this.IDM_ToGrayMemery.Name = "IDM_ToGrayMemery";
            this.IDM_ToGrayMemery.Size = new System.Drawing.Size(119, 22);
            this.IDM_ToGrayMemery.Text = "内存法";
            this.IDM_ToGrayMemery.Click += new System.EventHandler(this.IDM_ToGrayMemery_Click);
            // 
            // IDM_ToGrayPtr
            // 
            this.IDM_ToGrayPtr.Name = "IDM_ToGrayPtr";
            this.IDM_ToGrayPtr.Size = new System.Drawing.Size(119, 22);
            this.IDM_ToGrayPtr.Text = "指针法";
            this.IDM_ToGrayPtr.Click += new System.EventHandler(this.IDM_ToGrayPtr_Click);
            // 
            // 直方图ToolStripMenuItem
            // 
            this.直方图ToolStripMenuItem.Name = "直方图ToolStripMenuItem";
            this.直方图ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.直方图ToolStripMenuItem.Text = "直方图";
            // 
            // 图像增强ToolStripMenuItem
            // 
            this.图像增强ToolStripMenuItem.Name = "图像增强ToolStripMenuItem";
            this.图像增强ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.图像增强ToolStripMenuItem.Text = "图像增强";
            // 
            // 图像锐化ToolStripMenuItem
            // 
            this.图像锐化ToolStripMenuItem.Name = "图像锐化ToolStripMenuItem";
            this.图像锐化ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.图像锐化ToolStripMenuItem.Text = "图像锐化";
            // 
            // 图像剪切ToolStripMenuItem
            // 
            this.图像剪切ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.提取子图ToolStripMenuItem});
            this.图像剪切ToolStripMenuItem.Name = "图像剪切ToolStripMenuItem";
            this.图像剪切ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.图像剪切ToolStripMenuItem.Text = "图像剪切";
            // 
            // 提取子图ToolStripMenuItem
            // 
            this.提取子图ToolStripMenuItem.Name = "提取子图ToolStripMenuItem";
            this.提取子图ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.提取子图ToolStripMenuItem.Text = "提取子图";
            this.提取子图ToolStripMenuItem.Click += new System.EventHandler(this.提取子图ToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件ToolStripMenuItem,
            this.编辑ToolStripMenuItem,
            this.几何变换ToolStripMenuItem,
            this.灰度变换ToolStripMenuItem,
            this.图像剪切ToolStripMenuItem,
            this.频率变化ToolStripMenuItem,
            this.独自完成ToolStripMenuItem,
            this.增加功能ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1267, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 频率变化ToolStripMenuItem
            // 
            this.频率变化ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.小波变化ToolStripMenuItem});
            this.频率变化ToolStripMenuItem.Name = "频率变化ToolStripMenuItem";
            this.频率变化ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.频率变化ToolStripMenuItem.Text = "频率变化";
            // 
            // 小波变化ToolStripMenuItem
            // 
            this.小波变化ToolStripMenuItem.Name = "小波变化ToolStripMenuItem";
            this.小波变化ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.小波变化ToolStripMenuItem.Text = "小波变化";
            this.小波变化ToolStripMenuItem.Click += new System.EventHandler(this.小波变化ToolStripMenuItem_Click);
            // 
            // 独自完成ToolStripMenuItem
            // 
            this.独自完成ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.灰度ToolStripMenuItem,
            this.哈尔小波ToolStripMenuItem,
            this.绘画直方图ToolStripMenuItem,
            this.缩小ToolStripMenuItem,
            this.图片分割ToolStripMenuItem,
            this.缩小ToolStripMenuItem1,
            this.底片ToolStripMenuItem,
            this.原色ToolStripMenuItem,
            this.马赛克ToolStripMenuItem,
            this.锐化ToolStripMenuItem,
            this.平移ToolStripMenuItem,
            this.原色ToolStripMenuItem1,
            this.彩色ToolStripMenuItem,
            this.二值化ToolStripMenuItem});
            this.独自完成ToolStripMenuItem.Name = "独自完成ToolStripMenuItem";
            this.独自完成ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.独自完成ToolStripMenuItem.Text = "后续完成";
            this.独自完成ToolStripMenuItem.Click += new System.EventHandler(this.独自完成ToolStripMenuItem_Click);
            // 
            // 灰度ToolStripMenuItem
            // 
            this.灰度ToolStripMenuItem.Name = "灰度ToolStripMenuItem";
            this.灰度ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.灰度ToolStripMenuItem.Text = "灰度";
            this.灰度ToolStripMenuItem.Click += new System.EventHandler(this.灰度ToolStripMenuItem_Click);
            // 
            // 哈尔小波ToolStripMenuItem
            // 
            this.哈尔小波ToolStripMenuItem.Name = "哈尔小波ToolStripMenuItem";
            this.哈尔小波ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.哈尔小波ToolStripMenuItem.Text = "哈尔小波";
            this.哈尔小波ToolStripMenuItem.Click += new System.EventHandler(this.哈尔小波ToolStripMenuItem_Click);
            // 
            // 绘画直方图ToolStripMenuItem
            // 
            this.绘画直方图ToolStripMenuItem.Name = "绘画直方图ToolStripMenuItem";
            this.绘画直方图ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.绘画直方图ToolStripMenuItem.Text = "绘画直方图";
            this.绘画直方图ToolStripMenuItem.Click += new System.EventHandler(this.绘画直方图ToolStripMenuItem_Click);
            // 
            // 缩小ToolStripMenuItem
            // 
            this.缩小ToolStripMenuItem.Name = "缩小ToolStripMenuItem";
            this.缩小ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.缩小ToolStripMenuItem.Text = "网格处理";
            this.缩小ToolStripMenuItem.Click += new System.EventHandler(this.缩小ToolStripMenuItem_Click);
            // 
            // 图片分割ToolStripMenuItem
            // 
            this.图片分割ToolStripMenuItem.Name = "图片分割ToolStripMenuItem";
            this.图片分割ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.图片分割ToolStripMenuItem.Text = "图片分割";
            this.图片分割ToolStripMenuItem.Click += new System.EventHandler(this.图片分割ToolStripMenuItem_Click);
            // 
            // 缩小ToolStripMenuItem1
            // 
            this.缩小ToolStripMenuItem1.Name = "缩小ToolStripMenuItem1";
            this.缩小ToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.缩小ToolStripMenuItem1.Text = "浮雕";
            this.缩小ToolStripMenuItem1.Click += new System.EventHandler(this.缩小ToolStripMenuItem1_Click);
            // 
            // 底片ToolStripMenuItem
            // 
            this.底片ToolStripMenuItem.Name = "底片ToolStripMenuItem";
            this.底片ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.底片ToolStripMenuItem.Text = "底片";
            this.底片ToolStripMenuItem.Click += new System.EventHandler(this.底片ToolStripMenuItem_Click);
            // 
            // 原色ToolStripMenuItem
            // 
            this.原色ToolStripMenuItem.Name = "原色ToolStripMenuItem";
            this.原色ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.原色ToolStripMenuItem.Text = "马赛克";
            this.原色ToolStripMenuItem.Click += new System.EventHandler(this.原色ToolStripMenuItem_Click);
            // 
            // 马赛克ToolStripMenuItem
            // 
            this.马赛克ToolStripMenuItem.Name = "马赛克ToolStripMenuItem";
            this.马赛克ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.马赛克ToolStripMenuItem.Text = "漫画";
            this.马赛克ToolStripMenuItem.Click += new System.EventHandler(this.马赛克ToolStripMenuItem_Click);
            // 
            // 锐化ToolStripMenuItem
            // 
            this.锐化ToolStripMenuItem.Name = "锐化ToolStripMenuItem";
            this.锐化ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.锐化ToolStripMenuItem.Text = "锐化";
            this.锐化ToolStripMenuItem.Click += new System.EventHandler(this.锐化ToolStripMenuItem_Click);
            // 
            // 平移ToolStripMenuItem
            // 
            this.平移ToolStripMenuItem.Name = "平移ToolStripMenuItem";
            this.平移ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.平移ToolStripMenuItem.Text = "平滑";
            this.平移ToolStripMenuItem.Click += new System.EventHandler(this.平移ToolStripMenuItem_Click);
            // 
            // 原色ToolStripMenuItem1
            // 
            this.原色ToolStripMenuItem1.Name = "原色ToolStripMenuItem1";
            this.原色ToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.原色ToolStripMenuItem1.Text = "原色";
            this.原色ToolStripMenuItem1.Click += new System.EventHandler(this.原色ToolStripMenuItem1_Click);
            // 
            // 彩色ToolStripMenuItem
            // 
            this.彩色ToolStripMenuItem.Name = "彩色ToolStripMenuItem";
            this.彩色ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.彩色ToolStripMenuItem.Text = "彩色";
            this.彩色ToolStripMenuItem.Click += new System.EventHandler(this.彩色ToolStripMenuItem_Click);
            // 
            // 二值化ToolStripMenuItem
            // 
            this.二值化ToolStripMenuItem.Name = "二值化ToolStripMenuItem";
            this.二值化ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.二值化ToolStripMenuItem.Text = "二值化";
            this.二值化ToolStripMenuItem.Click += new System.EventHandler(this.二值化ToolStripMenuItem_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Location = new System.Drawing.Point(670, 28);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(585, 520);
            this.panel2.TabIndex = 3;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(583, 518);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(9, 565);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(948, 90);
            this.panel3.TabIndex = 4;
            // 
            // 增加功能ToolStripMenuItem
            // 
            this.增加功能ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.柔化ToolStripMenuItem,
            this.锐化ToolStripMenuItem1,
            this.雾化ToolStripMenuItem,
            this.光照ToolStripMenuItem,
            this.百叶窗ToolStripMenuItem,
            this.油画ToolStripMenuItem,
            this.堆积木ToolStripMenuItem});
            this.增加功能ToolStripMenuItem.Name = "增加功能ToolStripMenuItem";
            this.增加功能ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.增加功能ToolStripMenuItem.Text = "增加功能";
            // 
            // 柔化ToolStripMenuItem
            // 
            this.柔化ToolStripMenuItem.Name = "柔化ToolStripMenuItem";
            this.柔化ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.柔化ToolStripMenuItem.Text = "柔化";
            this.柔化ToolStripMenuItem.Click += new System.EventHandler(this.柔化ToolStripMenuItem_Click);
            // 
            // 锐化ToolStripMenuItem1
            // 
            this.锐化ToolStripMenuItem1.Name = "锐化ToolStripMenuItem1";
            this.锐化ToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.锐化ToolStripMenuItem1.Text = "锐化";
            this.锐化ToolStripMenuItem1.Click += new System.EventHandler(this.锐化ToolStripMenuItem1_Click);
            // 
            // 雾化ToolStripMenuItem
            // 
            this.雾化ToolStripMenuItem.Name = "雾化ToolStripMenuItem";
            this.雾化ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.雾化ToolStripMenuItem.Text = "雾化";
            this.雾化ToolStripMenuItem.Click += new System.EventHandler(this.雾化ToolStripMenuItem_Click);
            // 
            // 光照ToolStripMenuItem
            // 
            this.光照ToolStripMenuItem.Name = "光照ToolStripMenuItem";
            this.光照ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.光照ToolStripMenuItem.Text = "光照";
            this.光照ToolStripMenuItem.Click += new System.EventHandler(this.光照ToolStripMenuItem_Click);
            // 
            // 百叶窗ToolStripMenuItem
            // 
            this.百叶窗ToolStripMenuItem.Name = "百叶窗ToolStripMenuItem";
            this.百叶窗ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.百叶窗ToolStripMenuItem.Text = "百叶窗";
            this.百叶窗ToolStripMenuItem.Click += new System.EventHandler(this.百叶窗ToolStripMenuItem_Click);
            // 
            // 油画ToolStripMenuItem
            // 
            this.油画ToolStripMenuItem.Name = "油画ToolStripMenuItem";
            this.油画ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.油画ToolStripMenuItem.Text = "油画";
            this.油画ToolStripMenuItem.Click += new System.EventHandler(this.油画ToolStripMenuItem_Click);
            // 
            // 堆积木ToolStripMenuItem
            // 
            this.堆积木ToolStripMenuItem.Name = "堆积木ToolStripMenuItem";
            this.堆积木ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.堆积木ToolStripMenuItem.Text = "堆积木";
            this.堆积木ToolStripMenuItem.Click += new System.EventHandler(this.堆积木ToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1267, 667);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "BMP图像处理";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem 文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem IDM_FILE_OPEN;
        private System.Windows.Forms.ToolStripMenuItem 编辑ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 几何变换ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem IDM_IMGMOVE;
        private System.Windows.Forms.ToolStripMenuItem IDM_IMGVMORO;
        private System.Windows.Forms.ToolStripMenuItem IDM_IMGHMORO;
        private System.Windows.Forms.ToolStripMenuItem 旋转ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 缩放ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 灰度变换ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 黑白化ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem IDM_ToGrayGDI;
        private System.Windows.Forms.ToolStripMenuItem IDM_ToGrayMemery;
        private System.Windows.Forms.ToolStripMenuItem IDM_ToGrayPtr;
        private System.Windows.Forms.ToolStripMenuItem 直方图ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 图像增强ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 图像锐化ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 图像剪切ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 提取子图ToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ToolStripMenuItem 频率变化ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 小波变化ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 独自完成ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 灰度ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 哈尔小波ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 绘画直方图ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 缩小ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 图片分割ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 缩小ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 底片ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 原色ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 马赛克ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 锐化ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 平移ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 原色ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 彩色ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 二值化ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 增加功能ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 柔化ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 锐化ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 雾化ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 光照ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 百叶窗ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 油画ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 堆积木ToolStripMenuItem;
    }
}

