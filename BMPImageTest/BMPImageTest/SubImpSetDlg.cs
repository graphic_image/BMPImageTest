﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BMPImageTest
{
    public partial class SubImpSetDlg : Form
    {
        public SubImpSetDlg()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public int X
        {
            get { return Convert.ToInt32( x.Text); }
        }

        public int Y
        {
            get { return Convert.ToInt32(y.Text); }
        }

        public int W
        {
            get { return Convert.ToInt32(w.Text); }
        }

        public int H
        {
            get { return Convert.ToInt32(h.Text); }
        }
    }
}
