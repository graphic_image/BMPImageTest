﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BMPImageTest
{
    public partial class DlgPicuterMove : Form
    {
        public DlgPicuterMove()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public String XOffset
        {
            get { return xOffset.Text; }
        }

        public String YOffset
        {
            get { return yOffset.Text; }
        }
    }
}
